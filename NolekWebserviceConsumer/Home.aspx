﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="NolekWebserviceConsumer.Home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body onload="CheckSecurity()">
    <form id="form1" runat="server">
    <div>
        <table>  
            <tr>  
                <td>First Number:</td>  
                <td><input type="text" id="txtFirstNum" runat="server" /></td>  
                <td><p id="SecLevel"></p></td> 
            </tr>  
            <tr>  
                <td>Second Number:</td>  
                <td><input type="text" id="txtSecondNum" runat="server" /></td>  
            </tr>            
        </table>
        <table>  
            <tr>  
                <td><input type="button" onserverclick="AddNumber" value="Add" runat="server" /></td>                  
                <td><input type="button" onserverclick="SubNumber" value="Subtract" runat="server" /></td>                 
                <td><input type="button" onserverclick="MulNumber" value="Multiply" runat="server" /></td>  
                <td><input type="button" onserverclick="DivNumber" value="Divide" runat="server" /></td>  
                <td>Result:</td> 
                <td id="tdSum">
                    <div id="divSum" runat="server"></div>  
                    <div id="divSumThroughJson" runat="server"></div>
                </td>
            </tr> 
        </table>
        <table>  
            <tr>  
                <td><input type="button" onclick="SetSecurity(1)" value="Sec1"/></td>  
                <td><input type="button" onclick="SetSecurity(2)" value="Sec2"/></td>  
                <td><input type="button" onclick="SetSecurity(3)" value="Sec3"/></td>  
                <td><input type="button" onclick="SetSecurity(4)" value="Sec4"/></td>
            </tr> 
        </table>
    </div>
    </form>

    <%--<form action="demo_form.asp">
        First name: <input type="text" name="fname"><br>
        <input type="hidden" name="country" value="Norway">
        <input type="submit" value="Submit">
    </form>--%>

<script>
    function SetSecurity()
    {
        window.localStorage.setItem("SecurityLevel", arguments[0]);
        document.getElementById("SecLevel").innerHTML = "Security Level: " + arguments[0];
    }

    function CheckSecurity() {
        if (localStorage.getItem["SecurityLevel"]==null) {
            window.localStorage.setItem("SecurityLevel", 0);
        }
    }


</script>




</body>
</html>
