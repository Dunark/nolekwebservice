﻿using System;
using System.Web.Script.Serialization;

namespace NolekWebserviceConsumer
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        { }
        protected void AddNumber(object Sender, EventArgs E)
        {

            int Num1, Num2;
            string Key;
            int.TryParse(txtFirstNum.Value, out Num1);
            int.TryParse(txtSecondNum.Value, out Num2);
            //string.TryParse(txtSecondNum.Value, out Key);
            Key = txtSecondNum.ToString();

            //// creating object of ConversionService proxy class  
            //var ObjMyServiceProxy = new ConversionServiceSoapClient();

            ////var ObjMyServiceProxy = new ConversionServiceSoapClient();

            //// Invoke service method through service proxy  
            //divSum.InnerHtml = ObjMyServiceProxy.SubstractionOfNums(Num1, Num2).ToString();

            ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();

            divSum.InnerHtml = client.AdditionOfNums(Num1, Num2, Key).ToString();

        }
        protected void SubNumber(object Sender, EventArgs E)
        {
            int Num1, Num2;
            string Key;
            int.TryParse(txtFirstNum.Value, out Num1);
            int.TryParse(txtSecondNum.Value, out Num2);
            //string.TryParse(txtSecondNum.Value, out Key);
            Key = txtSecondNum.ToString();

            ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();
            //NolekWCFServiceReference.Service1Client client = new NolekWCFServiceReference.Service1Client();

            divSum.InnerHtml = client.SubstractionOfNums(Num1, Num2, Key).ToString();

        }
        protected void MulNumber(object Sender, EventArgs E)
        {

        }
        protected void DivNumber(object Sender, EventArgs E)
        {

        }

        protected void SetSecurity1(int level)
        {
        }


        //localStorage.getItem["audiourl"]

        //var ObjSumClass = new SumClass { First = Num1, Second = Num2 };
        //var ObjSerializer = new JavaScriptSerializer();
        //var JsonStr = ObjSerializer.Serialize(ObjSumClass);


        //divSumThroughJson.InnerHtml = ObjMyServiceProxy.GetSumThroughObject(JsonStr).Sum.ToString();

        //var Sum = ObjMyServiceProxy.SumOfNums1(JsonStr).Sum;
        //divSumThroughJson.InnerHtml = Sum.ToString();
    }
}