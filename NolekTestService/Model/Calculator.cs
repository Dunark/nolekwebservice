﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SecurityModule;


namespace NolekWCFService.Model
{
    public class Calculator
    {
        public int Num1 { get; set; }
        public int Num2 { get; set; }
        public string Result { get; set; }
        public int Key { get; set; }

        public static string AdditionOfInt(int a, int b, string Key)
        {
            if (SecurityMain.SecurityLevel(Key)>=1)
            {
                return (a + b).ToString();
            }
            else
            {
                return "You do not have the correct securitylevel";
            }
        }
        public static string SubstractionOfInt(int a, int b, string Key)
        {
            if (SecurityMain.SecurityLevel(Key) >= 2)
            {
                return (a - b).ToString();
            }
            else
            {
                return "You do not have the correct securitylevel";
            }
        }
        public static string MultiplicationOfInt(int a, int b, string Key)
        {
            if (SecurityMain.SecurityLevel(Key) >= 3)
            {
                return (a * b).ToString();
            }
            else
            {
                return "You do not have the correct securitylevel";
            }
        }
        public static string DivisionOfInt(int a, int b, string Key)
        {
            if (SecurityMain.SecurityLevel(Key) >= 4)
            {
                return (a / b).ToString();
            }
            else
            {
                return "You do not have the correct securitylevel";
            }
        }
    }
}