﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NolekHome.aspx.cs" Inherits="NolekWebConsumer.NolekHome" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body onload="CheckSecurity()">
    <form id="form1" runat="server">
    <div>
        <table>  
            <tr>  
                <td>First Number:</td>  
                <td><input type="text" id="txtFirstNum" runat="server" value="" /></td>  
                <td><p id="SecLevel"></p></td> 
            </tr>  
            <tr>  
                <td>Second Number:</td>  
                <td><input type="text" id="txtSecondNum" runat="server" value="" /></td>  
            </tr>            
        </table>
        <table>  
            <tr>  
                <td><input type="button" onserverclick="AddNumber" value="Add" runat="server" /></td>                  
                <td><input type="button" onserverclick="SubNumber" value="Subtract" runat="server" /></td>                 
                <td><input type="button" onserverclick="MulNumber" value="Multiply" runat="server" /></td>  
                <td><input type="button" onserverclick="DivNumber" value="Divide" runat="server" /></td>  
                <td>Result:</td> 
                <td id="tdSum">
                    <div id="divSum" runat="server"></div>  
                    <div id="divSumThroughJson" runat="server"></div>
                </td>
            </tr> 
        </table>
        <table>  
            <tr>  
                <td><input type="button" onclick="SetSecurity('Sec1')" value="Sec1"/></td>  
                <td><input type="button" onclick="SetSecurity('Sec2')" value="Sec2"/></td>  
                <td><input type="button" onclick="SetSecurity('Sec3')" value="Sec3"/></td>  
                <td><input type="button" onclick="SetSecurity('Sec4')" value="Sec4"/><input type="hidden" id="KeyField" runat="server" value=""/></td>
            </tr> 
        </table>
    </div>
    </form>

<script>
    function SetSecurity()
    {
        window.localStorage.setItem("SecurityLevel", arguments[0]);
        document.getElementById("KeyField").value = arguments[0];
        document.getElementById("SecLevel").innerHTML = "Security Level: " + arguments[0];
    }

    function CheckSecurity() {
        if (localStorage.getItem["SecurityLevel"] != null) {
            SetSecurity(localStorage.getItem["SecurityLevel"])
        }
         
        //if (localStorage.getItem["SecurityLevel"] == null)
        //{
        //    window.localStorage.setItem("SecurityLevel", 0);
        //    document.getElementById("KeyField").innerHTML = 0;
        //}
        //else
        //{
            //document.getElementById("KeyField").innerHTML = localStorage.getItem["SecurityLevel"]
        //}
    }


</script>
</body>
</html>
