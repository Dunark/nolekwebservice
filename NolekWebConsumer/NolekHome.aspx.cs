﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NolekWebConsumer
{
    public partial class NolekHome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        { }
        protected void AddNumber(object Sender, EventArgs E)
        {

            int Num1, Num2;
            string Key;
            int.TryParse(txtFirstNum.Value, out Num1);
            int.TryParse(txtSecondNum.Value, out Num2);
            Key = KeyField.Value;

            ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();

            divSum.InnerHtml = client.AdditionOfNums(Num1, Num2, Key);

        }
        protected void SubNumber(object Sender, EventArgs E)
        {
            int Num1, Num2;
            string Key;
            int.TryParse(txtFirstNum.Value, out Num1);
            int.TryParse(txtSecondNum.Value, out Num2);
            Key = KeyField.Value;

            ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();

            divSum.InnerHtml = client.SubstractionOfNums(Num1, Num2, Key);

        }
        protected void MulNumber(object Sender, EventArgs E)
        {
            int Num1, Num2;
            string Key;
            int.TryParse(txtFirstNum.Value, out Num1);
            int.TryParse(txtSecondNum.Value, out Num2);
            Key = KeyField.Value;

            ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();

            divSum.InnerHtml = client.MultiplicationOfNums(Num1, Num2, Key);
        }
        protected void DivNumber(object Sender, EventArgs E)
        {
            int Num1, Num2;
            string Key;
            int.TryParse(txtFirstNum.Value, out Num1);
            int.TryParse(txtSecondNum.Value, out Num2);
            Key = KeyField.Value;

            ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();

            divSum.InnerHtml = client.DivisionOfNums(Num1, Num2, Key);
        }

        protected void SetSecurity1(int level)
        {
        }
    }
}