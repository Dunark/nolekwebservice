﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace NolekWebApi.Controllers
{
    public class CalculatorController : Controller
    {
        // GET: Calc
        [HttpGet]
        public ActionResult Index()
        {
            NolekWCFService.Model.Calculator model = new NolekWCFService.Model.Calculator();
            //Models.CalcModel model = new Models.CalcModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Index(string submit, NolekWCFService.Model.Calculator model)
        {
            ModelState.Remove("Result");   // Result must update
            
            string Key = "Sec4";

            if (submit == "Add")
            {
                model.Result = NolekWCFService.Model.Calculator.AdditionOfInt(model.Num1, model.Num2, Key);
            }
            else if (submit == "Subtract")
            {
                model.Result = NolekWCFService.Model.Calculator.SubstractionOfInt(model.Num1, model.Num2, Key);
            }
            else if (submit == "Multiply")
            {
                model.Result = NolekWCFService.Model.Calculator.MultiplicationOfInt(model.Num1, model.Num2, Key);
            }
            else if (submit == "Divide")
            {
                model.Result = NolekWCFService.Model.Calculator.DivisionOfInt(model.Num1, model.Num2, Key);
            }

            ViewData["Result"] = model.Result;
            return View(model);
        }
    }
}