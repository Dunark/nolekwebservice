﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecurityModule
{
    class License
    {
        public string CompanyName { get; set; }
        public int CVR { get; set; }
        public string ContactPerson { get; set; }
        public int ContactPhoneNumber { get; set; }
        public int LicenseType { get; set; }
        public string LicenseKey { get; set; }
        public int PeopleregisteredOnLicense { get; set; }

        //List<user> usersRegistered = new List<user>();
    }
}
