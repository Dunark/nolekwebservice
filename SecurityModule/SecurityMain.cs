﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecurityModule
{
    public class SecurityMain
    {
        static private List<License> licenses = new List<License>();
        static void Main(string[] args)
        { 
        }

        public static int SecurityLevel(string securityToken)
        {
            if (securityToken == "Sec1")
            {
                return 1;
            }
            else if (securityToken == "Sec2")
            {
                return 2;
            }
            else if (securityToken == "Sec3")
            {
                return 3;
            }
            else if (securityToken == "Sec4")
            {
                return 4;
            }
            else
            {
                return 0;
            }
        }
    }
}
