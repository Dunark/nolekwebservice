﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NolekWebservice.Model
{
    public class Calculator
    {
        public static int AdditionOfInt(int a, int b)
        {
            return a + b;
        }
        public static int SubstractionOfInt(int a, int b)
        {
            return a - b;
        }
        public static int MultiplicationOfInt(int a, int b)
        {
            return a * b;
        }
        public static int DivisionOfInt(int a, int b)
        {
            return a / b;
        }
    }
}