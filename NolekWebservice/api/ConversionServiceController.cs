﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NolekWebservice.api
{
    public class ConversionServiceController : ApiController
    {
        [HttpGet]
        public int GetSumOfNums(int First, int Second)
        {
            return Model.Calculator.AdditionOfInt(First, Second);
        }
        [HttpGet]
        public int GetIncrement(int First)
        {
            return First+1;
        }

        [HttpGet]
        public int GetSessionCounter()
        {
            var httpContext = Request.Properties["MS_HttpContext"] as System.Web.HttpContextWrapper;
            int count = 0;
            if (httpContext.Session != null)
            {
                if (httpContext.Session["counter"] != null)
                    count = (int)httpContext.Session["counter"];
                ++count;
                httpContext.Session["counter"] = count;
            }
            return count;
        }


        // GET api/<controller>
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/<controller>
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}
    }
}