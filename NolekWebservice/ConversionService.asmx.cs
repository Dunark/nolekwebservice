﻿using System.Web.Services;

namespace NolekWebservice
{
    /// <summary>
    /// Summary description for ConversionService
    /// </summary>
    // Use "Namespace" attribute with an unique name,to make service uniquely discoverable  
    [WebService(Namespace = "http://tempuri.org/")]
    // To indicate service confirms to "WsiProfiles.BasicProfile1_1" standard,   
    // if not, it will throw compile time error.  
    [WebServiceBinding(ConformsTo = WsiProfiles.None)]    
    // To restrict this service from getting added as a custom tool to toolbox  
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX  
    [System.Web.Script.Services.ScriptService]
    public class ConversionService : System.Web.Services.WebService
    {

        [WebMethod]
        public int AdditionOfNums(int First, int Second)
        {
            return Model.Calculator.AdditionOfInt(First, Second);
        }

        [WebMethod]
        public int SubstractionOfNums(int First, int Second)
        {
            return Model.Calculator.SubstractionOfInt(First, Second);
        }

        [WebMethod]
        public int MultiplicationOfNums(int First, int Second)
        {
            return Model.Calculator.MultiplicationOfInt(First, Second);
        }

        [WebMethod]
        public int DivisionOfNums(int First, int Second)
        {
            return Model.Calculator.DivisionOfInt(First, Second);
        }

        // Takes a stringified JSON object & returns an object of SumClass  
        //[WebMethod(MessageName = "GetSumThroughObject")]
        //public SumClass SumOfNums(string JsonStr)
        //{
        //    var ObjSerializer = new JavaScriptSerializer();
        //    var ObjSumClass = ObjSerializer.Deserialize<SumClass>(JsonStr);
        //    return new SumClass().GetSumClass(ObjSumClass.First, ObjSumClass.Second);
        //}
    }

    // Normal class, an instance of which will be returned by service  
    //public class SumClass
    //{
    //    public int First, Second, Sum;

    //    public SumClass GetSumClass(int Num1, int Num2)
    //    {
    //        var ObjSum = new SumClass
    //        {
    //            Sum = Num1 + Num2,
    //        };
    //        return ObjSum;
    //    }
    //}


}

