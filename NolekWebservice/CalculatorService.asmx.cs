﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace NolekWebservice
{
    /// <summary>
    /// Summary description for CalculatorService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class CalculatorService : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }
        [WebMethod]
        public int AdditionOfNums(int First, int Second)
        {
            return Model.Calculator.AdditionOfInt(First, Second);
        }

        [WebMethod]
        public int SubstractionOfNums(int First, int Second)
        {
            return Model.Calculator.SubstractionOfInt(First, Second);
        }

        [WebMethod]
        public int MultiplicationOfNums(int First, int Second)
        {
            return Model.Calculator.MultiplicationOfInt(First, Second);
        }

        [WebMethod]
        public int DivisionOfNums(int First, int Second)
        {
            return Model.Calculator.DivisionOfInt(First, Second);
        }
    }
}
